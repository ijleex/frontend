-- http://blog.csdn.net/qq_33290787/article/details/51899042

-- MySQL 使用ROW_COUNT()返回插入、更新、删除操作影响行数

DELIMITER $$
DROP PROCEDURE IF EXISTS `execSecHit`$$
CREATE PROCEDURE execSecHit(IN hitId   VARCHAR(36), IN hitsID VARCHAR(36), IN mobileNo VARCHAR(36),
                            IN hitsTime TIMESTAMP, OUT hitsResult INT)
    BEGIN
        DECLARE `@modCount` INT DEFAULT 0;
        START TRANSACTION;
        INSERT IGNORE t_frontend_hit_records (id, hit_id, user_id, state, create_time)
        VALUES (hitId, hitsID, mobileNo, 1, hitsTime);
        SELECT ROW_COUNT() INTO `@modCount`;
        IF (`@modCount` = 0)
        THEN
            ROLLBACK;
            SET hitsResult = -1;
        ELSEIF (`@modCount` < 0)
            THEN
                ROLLBACK;
                SET hitsResult = -2;
        ELSE
            UPDATE t_frontend_goods SET quantity = quantity - 1
            WHERE id = hitsID AND start_time < hitsTime AND end_time > hitsTime AND quantity > 0;
            SELECT ROW_COUNT() INTO `@modCount`;
            IF (`@modCount` = 0)
            THEN
                ROLLBACK;
                SET hitsResult = 0;
            ELSEIF (`@modCount` < 0)
                THEN
                    ROLLBACK;
                    SET hitsResult = -2;
            ELSE
                COMMIT;
                SET hitsResult = 1;
            END IF;
        END IF;
    END $$
DELIMITER ;

-- --------------------------------------------------------------------
SET @id = '100000001';
SET @hitsID = '01';
SET @mobileNo = '15812345678';
SET @hitsTime = NOW();
SET @hitsResult = -3;
CALL execSecHit(@id,@hitsID,@mobileNo,@hitsTime,@hitsResult);
SELECT @hitsResult;

-- hitsResult:
--  0 秒杀已经关闭
--  1 秒杀成功
-- -1 重复秒杀
-- -2 内部错误

-- --------------------------------------------------------------------
DROP TABLE t_frontend_goods;
CREATE TABLE t_frontend_goods (
    id VARCHAR(36) COMMENT '主键',
    name VARCHAR(50) NOT NULL COMMENT '名称',
    quantity INT DEFAULT 0 NOT NULL COMMENT '数量',
    start_time TIMESTAMP DEFAULT NOW() NOT NULL COMMENT '开始日期',
    end_time TIMESTAMP DEFAULT NOW() NOT NULL COMMENT '开始日期',
    version TIMESTAMP DEFAULT NOW() NOT NULL COMMENT '版本',
    CONSTRAINT pk_frontend_goods PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品';

DROP TABLE t_frontend_hit_records;
CREATE TABLE t_frontend_hit_records (
    id VARCHAR(36) COMMENT '主键',
    hit_id VARCHAR(36)NOT NULL COMMENT '名称',
    user_id VARCHAR(36)NOT NULL COMMENT '用户ID',
    state INT DEFAULT 1 NOT NULL COMMENT '状态',
    create_time TIMESTAMP DEFAULT NOW() NOT NULL COMMENT '日期',
    CONSTRAINT pk_frontend_hit_records PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='秒杀';

ALTER TABLE t_frontend_hit_records ADD CONSTRAINT uk_frontend_hit_records UNIQUE KEY(hit_id, user_id);

INSERT INTO t_frontend_goods VALUES ('01', 'name-01', 100, NOW(), NOW() + INTERVAL 7 DAY, NOW());
INSERT INTO t_frontend_goods VALUES ('02', 'name-02', 50, NOW(), NOW() + INTERVAL 10 DAY, NOW());

COMMIT;

-- --------------------------------------------------------------------
SET @hitTime='2017-05-04 22:28:57';
SELECT * FROM t_frontend_goods WHERE id = '01' AND start_time < @hitTime AND end_time > @hitTime AND quantity > 0;

-- -----------------------
SET @id='100000001';
SET @hitsID='01'; -- goods id
SET @mobileNo='15812345678';
SET @hitsTime='2017-05-04 22:28:57';
INSERT IGNORE t_frontend_hit_records(id,hit_id,user_id,state,create_time) VALUES(@id,@hitsID,@mobileNo,1,@hitsTime);
UPDATE t_frontend_goods
SET quantity = quantity - 1, version = NOW()
WHERE id = @hitsID AND start_time < @hitsTime AND end_time > @hitsTime AND quantity > 0;

COMMIT;
