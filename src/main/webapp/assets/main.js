/*!
 * Created by 李远明 on 2017-04-19 22:05.
 */
(function() {
    require.config({
        "baseUrl": "assets",
        paths: {
            "jquery": ["//cdn.bootcss.com/jquery/2.2.4/jquery", "jquery"],
            "bootstrap": "bootstrap/js/bootstrap.min",
            "jquery.validate": "jquery-validation/jquery.validate.min",
            "validate": "jquery-validation/additional-methods",
            "serializeJSON": "jquery.serializeForm/jquery.serializejson",
            "jsencrypt": "jsencrypt/2.3.1/jsencrypt",
            "password.check": "scripts/password.check",
            "jquery.progressbar": "scripts/jquery.progressbar.min",
            "Util": "scripts/utils"
        },
        shim: {
            "bootstrap": {"deps": ["jquery"]},
            "jquery.validate": {"deps": ["jquery"]},
            "validate": {"deps": ["jquery", "jquery.validate"]},
            "serializeJSON": {"deps": ["jquery"]},
            "jsencrypt": {exports: ["jsencrypt"]}
        },
        urlArgs: 'v=20170621', // 版本号
        waitSeconds: 15
    });

    var head = document.getElementsByTagName("head")[0]; // document.head
    var favicon = document.createElement("link");
    favicon.rel = "shortcut icon";
    favicon.type = "image/x-icon";
    favicon.href = "assets/img/favicon.ico";
    head.appendChild(favicon);
})();

