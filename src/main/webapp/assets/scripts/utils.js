/**
 * Created by 李远明 on 2017-04-20 11:19.
 */
define("Util", [], function() {

    var Cookie = {
        set: function(name, value, expire) {
            var exp = new Date();
            exp.setTime(exp.getTime() + expire * 24 * 60 * 60 * 1000);
            var cookie = name + "=" + encodeURIComponent(value) + ";expires=" + exp.toUTCString() + ";domain=localhost;path=/frontend";
            document.cookie = cookie;
        },
        get: function(key) {
            var cookies = document.cookie ? document.cookie.split('; ') : [];
            for (var i = 0, l = cookies.length; i < l; i++) {
                var parts = cookies[i].split('=');
                var name = parts.shift();
                var cookie = parts.join('=');
                if (key && key === name) {
                    return cookie;
                }
            }
        },
        setALCookie: function() {
            if ($("input[name='rememberMe']").prop("checked")) {
                var days = 3 * 30;
                this.set("alpin", $("#form-username").val(), days);
            } else {
                var v = this.get("alpin");
                if (v) {
                    this.set("alpin", "", -100);
                }
            }
        }
    };
    var Header = {
        getProtocol: function() {
            var parentScheme = '';
            try {
                parentScheme = parent.location.protocol;
            } catch (e) {
            }
            if (!parentScheme) {
                var referer = document.referrer;
                if (referer.indexOf(LoginConstant.HTTPS_SCHEME) === 0) {
                    parentScheme = LoginConstant.HTTPS_SCHEME;
                } else {
                    parentScheme = LoginConstant.HTTP_SCHEME;
                }
            }
            return parentScheme;
        }
    };

    return {
        getURLParameter: function(paramName) {
            var searchString = window.location.search.substring(1),
                i, pair, params = searchString.split("&"), l = params.length;

            for (i = 0; i < l; i++) {
                pair = params[i].split("=");
                if (pair[0] === paramName) {
                    var val = pair[1];
                    //return unescape(val[1]);
                    // 使用 decodeURIComponent() 方法替换 unescape() 方法 李远眀 2017-02-22 22:56:44
                    return decodeURIComponent(val);
                }
            }
            return null;
        },
        Cookie: Cookie
    };

});
