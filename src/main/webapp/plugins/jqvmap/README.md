![JQVMap](http://jqvmap.com/img/logo.png "JQVMap")

JQVMAP 可以方便的帮助你生成漂亮的矢量地图。

http://jvectormap.com/

https://github.com/manifestinteractive/jqvmap
https://github.com/manifestinteractive/jqvmap/zipball/master


<script src="js/jquery.vmap.js"></script>
<script src="js/jquery.vmap.world.js"></script>
<script src="js/jquery.vmap.sampledata.js"></script>
 
<script>
jQuery('#vmap').vectorMap({
    map: 'world_en',
    backgroundColor: null,
    color: '#ffffff',
    hoverOpacity: 0.7,
    selectedColor: '#666666',
    enableZoom: true,
    showTooltip: true,
    values: sample_data,
    scaleColors: ['#C8EEFF', '#006491'],
    normalizeFunction: 'polynomial'
});
</script>
 
<div id="vmap" style="width: 600px; height: 400px;"></div>

