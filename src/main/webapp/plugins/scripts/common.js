/**
 * 获取表单数据
 * <p>
 * @see jquery.serializeObject.js: serializeObject()
 * @param formId 表单id
 * @return 表单数据对象
 */
function getFormData(formId) {
    var formData = $('#' + formId).serializeArray();
    var ret = {};
    $.each(formData, function() {
        if (ret[this.name] !== undefined) {
            if (!ret[this.name].push) {
                ret[this.name] = [ret[this.name]];
            }
            ret[this.name].push(this.value || '');
        } else {
            ret[this.name] = this.value || '';
        }
    });
    return ret;
}
/**
 * 从数组中根据键与值查找对象
 * <p>
 * var dict = [{"text":"状态1","value":"1"},{"text":"状态2","value":"2"},{"text":"状态3","value":"3"}]; <br>
 * var ret = findObjectByKey(dict, 'value', '2');
 * 
 * @param array 数组
 * @param key 键
 * @param value 值
 * @return 数组中 'value'==value 的对象
 */
function findObjectByKey(array, key, value) {
    for (var i = 0, l = array.length; i < l; i++) {
        if (array[i][key] === value) {
            return array[i];
        }
    }
    return null;
}
/**
 * 查找 select 组件指定值得 label 的值
 * <p>
 * 当前使用 bootstrap-multiselect 生成 select 的option，结构如下：
 * <br>
 * <select class="form-control select-userState" name="state">
 * 	<option value="" label="请选择…">请选择…</option>
 * 	<option value="1" label="状态1">状态1</option>
 * 	<option value="2" label="状态2">状态2</option>
 * </select>
 * 
 * @param selectClass select的class，如select-userState
 * @param value option的值
 * @return label标签对应的值
 */
function findSelectOption(selectClass, value) {
	// $(".select-roleState option[value='1']")
    var option = $("." + selectClass + " option[value='" + value + "']");
    var text = option.attr('label');
    return text === undefined ? value : text;
}
(function($) {
    /**
	 * 为组件添加 setFocus 方法，用于设置焦点。
	 */
    jQuery.fn.setFocus = function() {
        return this.each(function() {
            var dom = this;
            setTimeout(function() {
                try {
                    dom.focus();
                } catch (e) {}
            }, 0);
        });
    }
})(jQuery);
