/**
 * Created by 李远明 on 2017-03-09 16:40.
 */
function getXMLHttpRequest() {
	var xhr;
	if (window.ActiveXObject) {
		xhr = new ActiveXObject("Microsoft.XMLHTTP");
	} else if (window.XMLHttpRequest) {
		xhr = new XMLHttpRequest();
	} else {
		xhr = null;
	}
	return xhr;
}
/**
 * 发送（原始）AJAX请求
 */
function sendAjax() {
	var xhr = getXMLHttpRequest();
	xhr.open("POST", "public/test/testRawAjax");
	// 原始AJAX请求的 Content-Type 为 text/plain;charset=UTF-8
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
	var data = "userNo=000009&username=Tom";
	xhr.send(data);
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200) {
			console.log("return:" + xhr.responseText);
		}
	};
}

// 调用
sendAjax();
