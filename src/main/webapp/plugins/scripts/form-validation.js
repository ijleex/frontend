/* 表单验证
 * For more info visit the official plugin documentation: 
 *  http://docs.jquery.com/Plugins/Validation
 */
var FormValidation = function() {

	// basic validation
	var handleValidation = function(formId) {

		var form = $('#' + formId);
		var error = $('.alert-danger', form);
		var success = $('.alert-success', form);

		form.validate({
			errorElement : 'span', // default input error message container
			errorClass : 'help-block help-block-error', // default input error message class
			focusInvalid : false, // do not focus the last invalid input
			ignore : "", // validate all fields including form hidden input
			rules : {},

			// display error alert on form submit
			invalidHandler : function(event, validator) {
				success.hide();
				error.show();
				Metronic.scrollTo(error, -200);
			},

			highlight : function(element) { // hightlight error inputs
				// set error class to the control group
				$(element).closest('.form-group').addClass('has-error');
			},

			// revert the change done by hightlight
			unhighlight : function(element) {
				// set success class to the control group
				$(element).closest('.form-group').removeClass('has-error');
			},

			success : function(label) {
				label.closest('.form-group').removeClass('has-error');
			},

			submitHandler : function(form) {
				//success.show();
				error.hide();
			}
		});
	}

	var handleWysihtml5 = function() {
		if (!jQuery().wysihtml5) {
			return;
		}

		if ($('.wysihtml5').size() > 0) {
			$('.wysihtml5').wysihtml5({
				"stylesheets" : [ "plugins/bootstrap3-wysiwyg/wysiwyg-color.css" ]
			});
		}
	}

	/**
	 * 关闭模态窗口时，清空表单数据
	 * 
	 * @version 2015-12-23 16:09 李远明
	 */
	var resetModalForm = function() {
		$(".modal").on("hide.bs.modal", function() {
			var fms = $(this).find('form');
			$(fms).each(function(){
				this.reset();
			});
		});
	}

	// main function to initiate the module
	return {
		init : function(formId) {
			// handleWysihtml5();
			handleValidation(formId);
			resetModalForm();
		}
	};

}();
