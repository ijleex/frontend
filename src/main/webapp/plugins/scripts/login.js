var Login = function() {

	var handleLogin = function() {
		$("#form_login").validate({
			errorElement: 'span', // default input error message container
			errorClass: 'help-block', // default input error message class
			focusInvalid: false, // do not focus the last invalid input
			rules: {
				username: {
					required: true
				},
				password: {
					required: true
				},
                rememberMe: {
					required: false
				}
			},
			messages: {
				username: {
					required: "请输入用户名",
					minlength: '用户名不能少于 4 个字符'
				},
				password: {
					required: "请输入密码",
					minlength: '密码不能少于 6 个字符'
				}
			},
			invalidHandler: function(event, validator) { // display error alert on form submit
				$('.alert-danger', $('.login-form')).show();
			},
			highlight: function(element) { // hightlight error inputs
				$(element).closest('.form-group').addClass('has-error'); // set error class to the control group
			},
			success: function(label) {
				label.closest('.form-group').removeClass('has-error');
				label.remove();
			},
			errorPlacement: function(error, element) {
				error.insertAfter(element.closest('.input-icon'));
			},
			submitHandler: function(form) {
				form.submit(); // form validation success, call ajax form submit
			}
		});
		$("#form_login").find('input').keypress(function(e) {
			if (e.which === 13) {
				var form = $("#form_login");
				if (form.validate().form()) {
					form.submit(); // form validation success, call ajax form submit
				}
				return false;
			}
		});
	};

	return {
		// main function to initiate the module
		init: function() {
			handleLogin();
		}
	};

}();
