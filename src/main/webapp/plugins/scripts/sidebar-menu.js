/**
 * 树状菜单：
 * <li class="start active "><a href="index.html"><i class="icon-home"></i><span class="title">主面板</span><span class="selected"></span></a></li>
 * <li><a href="javascript:;"><i class="icon-basket"></i><span class="title">电子商务</span><span class="arrow"></span></a>
 *    <ul class="sub-menu">
 *        <li><a href="ecommerce_index.html"><i class="icon-home"></i> Dashboard </a></li>
 *        <li><a href="ecommerce_orders.html"><i class="icon-basket"></i> 订单</a></li>
 *        <li><a href="ecommerce_orders_view.html"><i class="icon-tag"></i> 订单查看</a></li>
 *        <li><a href="ecommerce_products.html"><i class="icon-handbag"></i> 商品列表</a></li>
 *        <li><a href="ecommerce_products_edit.html"><i class="icon-pencil"></i> 商品编辑</a></li>
 *    </ul>
 * </li>
 * ...
 */
$.ajax('menu/tree/getByUser/sysmgmt', {
	type: 'GET',
	data: {"_": (+new Date())},
	cache: true,
	dataType: 'JSON'
}).done(function(data) {
	var menuContent = '<li class="start active "><a href="index.html"><i class="icon-home"></i><span class="title">主面板</span><span class="selected"></span></a></li>';
	var defIcon = 'icon-folder';

	var children = data.message.children;
	$.each(children, function(i, node) {
		var children = node.children;
		if (children != null) { // 分枝结点（有子菜单，说明是标题）
			menuContent += '<li><a href="javascript:;"><i class="' + node.iconClass + '"></i><span class="title">' + node.name + '</span><span class="arrow"></span></a>';
			menuContent += '<ul class="sub-menu">';
			$.each(children, function(j, leaf) { // 遍历子菜单
				menuContent += '<li><a href="javascript:;" title="' + leaf.name + '" data-url="' + leaf.uri + '" onclick="loadContent(this)">';
				var iconClass = leaf.iconClass;
				if (iconClass != null) {
					menuContent += '<i class="' + iconClass + '"></i>';
				}
				menuContent += leaf.name + '</a></li>';
			});
			menuContent += '</ul></li>';
		} else { // 叶子结点
			menuContent += '<li><a href="javascript:;" title="' + node.name + '" data-url="' + node.uri + '" onclick="loadContent(this)">';
			var iconClass = node.iconClass;
			if (iconClass != null) {
				menuContent += '<i class="' + iconClass + '"></i>';
			}
			menuContent += node.name + '</a></li>';
		}
	});
	$('#sidebar-menu').html(menuContent);
}).fail(function(err) {
	console.log("失败回调");
});

function loadContent(obj) {
	//var title = obj.title;
	//console.log('title: ' + title);
	var url = obj.dataset.url;
	$('#page-content').load(url);
}
