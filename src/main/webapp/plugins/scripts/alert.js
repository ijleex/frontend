/**
 * toastr.js
 * 
 * 在右下角弹出：succ、info、warn、error等信息；确认框请使用 bootbox.confirm 。
 */
var Alert = function() {

	/*
	 * 参数设置
	 * 
	 * 弹出窗位置： toast-top-right toast-bottom-right toast-bottom-left toast-top-left toast-top-center
	 * toast-bottom-center toast-top-full-width toast-bottom-full-width
	 */
	toastr.options = {
		"closeButton" : false, // 是否显示关闭按钮
		"debug" : false, // 是否使用debug模式
		"positionClass" : "toast-bottom-right",// 弹出窗位置
		"onclick" : null,
		"showDuration" : "300",// 显示时间
		"hideDuration" : "1000",// 消失时间
		"timeOut" : "5000", // 展现时间
		"extendedTimeOut" : "1000",// 加长展示时间
		"showEasing" : "swing",// 显示时的动画缓冲方式：swing, linear
		"hideEasing" : "linear",// 消失时的动画缓冲方式：swing, linear
		"showMethod" : "slideDown",// 显示时的动画方式：show, fadeIn, slideDown
		"hideMethod" : "slideUp" // 消失时的动画方式：hide, fadeOut, slideUp
	};

	return {
		succ : function(msg) {
			toastr.success(msg, '成功');
		},
		info : function(msg) {
			toastr.info(msg, '提示');
		},
		warn : function(msg) {
			toastr.warning(msg, '警告');
		},
		error : function(msg) {
			toastr.error(msg, '错误');
		},
		clear : function() {
			toastr.clear();
		}
	}
}();
