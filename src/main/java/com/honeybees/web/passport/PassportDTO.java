package com.honeybees.web.passport;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * <dl>
 * <dt><b> 标题 </b></dt>
 * <p>
 * <dd>功能描述</dd>
 * </dl>
 * <p>
 * Copyright (c) All rights reserved.
 * </p>
 *
 * @author 李远明
 * @version 2017-04-15 12:56
 */
public class PassportDTO implements Serializable {

    private static final long serialVersionUID = -2107277784881063596L;

    /**
     * 个人标识号 (PIN=personal identification number)
     */
    private String pin;
    private String username;
    private String email;
    private String mobileNo;
    private String password;
    private String chkPassword;
    /**
     * 手机短信验证码
     *
     * @since 2017-06-21 15:49
     */
    private String mobileCode;
    /**
     * 验证码
     */
    private String captchaCode;
    /**
     * 已缓存的验证码
     *
     * @since 2017-03-13 21:05:55 准备将PassportController中的业务代码移到一个新建的Service中
     */
    private String cacheCaptchaCode;
    /**
     * 记住我（自动登录）：0-否，1-是
     *
     * @since 2017-03-12 21:39:37
     */
    private int rememberMe;

    /**
     * @since 2017-03-14 21:14:19 添加JS加密，登录时，返回公钥，用于计算SHA256，获取私钥
     */
    private String pubKey;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileCode() {
        return mobileCode;
    }

    public void setMobileCode(String mobileCode) {
        this.mobileCode = mobileCode;
    }

    public String getChkPassword() {
        return chkPassword;
    }

    public void setChkPassword(String chkPassword) {
        this.chkPassword = chkPassword;
    }

    public String getCaptchaCode() {
        return captchaCode;
    }

    public void setCaptchaCode(String captchaCode) {
        this.captchaCode = captchaCode;
    }

    public String getCacheCaptchaCode() {
        return cacheCaptchaCode;
    }

    public void setCacheCaptchaCode(String cacheCaptchaCode) {
        this.cacheCaptchaCode = cacheCaptchaCode;
    }

    public int getRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(int rememberMe) {
        this.rememberMe = rememberMe;
    }

    public String getPubKey() {
        return pubKey;
    }

    public void setPubKey(String pubKey) {
        this.pubKey = pubKey;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
