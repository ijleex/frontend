package com.honeybees.web.passport.service.impl;

import com.honeybees.framework.spring.mvc.model.ReturnMessage;
import com.honeybees.web.passport.PassportDTO;
import com.honeybees.web.passport.service.PassportService;
import org.springframework.stereotype.Service;

/**
 * 标题
 * <p>
 * 功能描述
 *
 * @author 李远明
 * @since 2017-04-26 15:26 新建
 */
@Service
public class PassportServiceImpl implements PassportService {

    @Override
    public ReturnMessage login(PassportDTO object) {
        ReturnMessage returnMessage = ReturnMessage.DONE_MESSAGE;

        returnMessage.setMessage("登录成功");
        return returnMessage;
    }

    @Override
    public ReturnMessage register(PassportDTO object) {
        return null;
    }

    @Override
    public ReturnMessage isPinUsed(PassportDTO object) {
        return null;
    }

    @Override
    public ReturnMessage isShowCaptchaImage(PassportDTO object) {
        return null;
    }
}
