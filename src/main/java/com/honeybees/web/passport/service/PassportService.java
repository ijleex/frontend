package com.honeybees.web.passport.service;

import com.honeybees.framework.spring.mvc.model.ReturnMessage;
import com.honeybees.web.passport.PassportDTO;

/**
 * 登录、注册相关
 * <p>
 *
 * @author 李远明
 * @since 2017-04-20 17:43 新建
 */
public interface PassportService {

    /**
     * 登录
     *
     * @param object 登录信息
     * @return 返回信息
     * @since 2017-04-20 17:44:09
     */
    ReturnMessage login(PassportDTO object);

    /**
     * 注册
     *
     * @param object 注册信息
     * @return 返回信息
     * @since 2017-04-20 17:54:22
     */
    ReturnMessage register(PassportDTO object);

    /**
     * 判断注册名是否已使用
     *
     * @param object 注册信息
     * @return 返回信息
     * @since 2017-04-20 17:54:22
     */
    ReturnMessage isPinUsed(PassportDTO object);

    /**
     * 是否显示图片验证码
     *
     * @param object PIN信息
     * @return 返回信息
     * @since 2017-04-20 17:54:22
     */
    ReturnMessage isShowCaptchaImage(PassportDTO object);

}
