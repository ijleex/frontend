package com.honeybees.web.passport.action;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.honeybees.framework.spring.mvc.WebController;
import com.honeybees.framework.spring.mvc.model.ReturnMessage;
import com.honeybees.framework.sso.session.manager.MockSessionManager;
import com.honeybees.framework.sso.vo.LoginUser;
import com.honeybees.web.captcha.cache.CaptchaCacheHandler;
import com.honeybees.web.passport.PassportDTO;
import com.honeybees.web.passport.service.PassportService;

/**
 * <dl>
 * <dt><b> 登录、注册等 </b></dt>
 * <p>
 * <dd>功能描述</dd>
 * </dl>
 * <p>
 * Copyright (c) All rights reserved.
 * </p>
 *
 * @author 李远明
 * @version 2017-04-15 12:35
 */
@RequestMapping("/passport")
@Controller
public class PassportAction extends WebController<ReturnMessage> {

    @Autowired
    private MockSessionManager mockSessionManager;
    @Autowired
    private CaptchaCacheHandler cacheHandler;

    @Autowired
    private PassportService passportService;

    /**
     * 登录
     *
     * @param request 当前请求
     * @param dto 登录信息
     * @return 信息
     * @since 2017-04-15 12:37:28
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public ReturnMessage login(HttpServletRequest request, PassportDTO dto) {
        String sessionId = super.getSessionId(request);
        String captchaCode = this.cacheHandler.getCaptchaCode(sessionId);
        dto.setCacheCaptchaCode(captchaCode);

        ReturnMessage result = this.passportService.login(dto);
        if (!result.isDone()) {
            return result;
        }

        Object message = result.getMessage();
        LoginUser user = (LoginUser) message;
        this.mockSessionManager.createSession(user, sessionId, null);
        return DONE_MESSAGE;
    }

}
