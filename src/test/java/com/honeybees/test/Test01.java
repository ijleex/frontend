/**
 * 
 */
package com.honeybees.test;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * <dl>
 * <dt><b> 标题 </b></dt>
 * <p>
 * <dd>功能描述</dd>
 * </dl>
 * <p>
 * Copyright (C) All rights reserved.
 * </p>
 * 
 * @author 李远明
 * @version 2015年11月28日 下午4:51:35
 * @version %I%, %G%
 */
public class Test01 {

	public static void main(String[] args) {
		Date date = new Date(1448630865146L);
		DateFormat dateFormat = DateFormat.getDateTimeInstance();
		System.out.println(dateFormat.format(date));

		Calendar c = Calendar.getInstance();
		c.set(2015, 3, 16); // 2015-4-16
		c.set(Calendar.MONTH, 4); // 5 月
		System.out.println(dateFormat.format(c.getTime()));

		DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, Locale.CHINA);
		System.out.println(formatter.format(c.getTime()));
	}

}
